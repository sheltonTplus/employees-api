# BUILD
FROM maven:3.9.2-eclipse-temurin-17-alpine as maven

# Crie um diretório de trabalho e copie o código fonte
WORKDIR /code
COPY . .

# Instale dependências e compile a aplicação
RUN apk add --no-cache libstdc++ && \
    mvn --quiet clean package -DskipTests

# Crie a imagem final
FROM amazoncorretto:17.0.7-alpine

WORKDIR /code

# Copie o JAR da fase de construção
COPY --from=maven /code/target/*.jar /code/app.jar

EXPOSE 8080
CMD ["java", "-jar", "/code/app.jar"]
