<h1 align="center">
    Employee REST API
</h1>
<p align="center">
 <img src="https://img.shields.io/static/v1?label=ONIT&message=Desafio&color=8257E5&labelColor=555" alt="Challenge" />
</p>

### About 🔥
A Simple Employees Rest API  

### Techs 👨‍💻
For further reference, please consider the following sections:

* [Official Apache Maven](https://maven.apache.org/guides/index.html)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/3.2.2/reference/htmlsingle/index.html#howto.data-initialization.migration-tool.flyway)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.2.2/reference/htmlsingle/index.html#web)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.2.2/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)
* [SpringDoc OpenAPI 3](https://springdoc.org/v2/#spring-webflux-support)
* [PostgreSQL]()
* [Docker]()

### Features ✅
* [x] Create an employee
* [x] Find an employee by id
* [x] Get all employees
* [x] Filter employees by filters (name, email, bi, nuit)
* [x] Update employee
* [x] Delete employee

### Deployments ✨
- [Web Service(deployed on render.com)](https://employees-api-6gx2.onrender.com/swagger-ui/)
- [Production database(hosted on railway.com)](https://railway.app/project/2733990f-f08b-4fc8-bb1c-b26c8b515814/service/7df4e44b-5e33-4db5-b571-a584f0fb58ba/data)
- [Test database(hosted on railway.com)](https://railway.app/project/2733990f-f08b-4fc8-bb1c-b26c8b515814/service/673e22b8-541d-4d7a-809c-7a23a3b10549/data)