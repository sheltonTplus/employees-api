package dev.sheltonfrancisco.onit.employeesapi.employee.model;

import dev.sheltonfrancisco.onit.employeesapi.utils.EnumTypes;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Entity
@Table(name = "employees")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@SQLDelete(sql = "UPDATE employees SET deleted_at = NOW() WHERE id = ?")
@SQLRestriction("deleted_at IS NULL")
public class Employee {
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        private Long id;
        private String name;
        private String surname;
        private LocalDate birthDate;
        private String bi;
        private String nuit;
        private String phoneNumber;
        private String email;
        @Enumerated(EnumType.STRING)
        private EnumTypes.Genre genre;
        @Enumerated(EnumType.STRING)
        private EnumTypes.MaritalStatus maritalStatus;
        @CreationTimestamp
        private LocalDateTime createdAt;
        @UpdateTimestamp
        private LocalDateTime updatedAt;
        private LocalDateTime deletedAt;
}