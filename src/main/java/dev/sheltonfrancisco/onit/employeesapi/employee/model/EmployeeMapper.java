package dev.sheltonfrancisco.onit.employeesapi.employee.model;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.UpdateEmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.presentation.EmployeeJson;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

@Mapper
public abstract class EmployeeMapper {

    public final static EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);
    public abstract Employee toModel(EmployeeRequest request);
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public abstract void updateEmployeeFromRequest(UpdateEmployeeRequest request, @MappingTarget Employee employee);

    @Mapping(source = "genre.description", target = "genre")
    @Mapping(source = "maritalStatus.description", target = "maritalStatus")
    public abstract EmployeeJson toJson(Employee employee);

    public  Page<EmployeeJson> toJson(Page<Employee> employees) {
        return employees.map(this::toJson);
    }
}
