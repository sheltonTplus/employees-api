package dev.sheltonfrancisco.onit.employeesapi.employee.model.request;

import dev.sheltonfrancisco.onit.employeesapi.utils.EnumTypes;

public record EmployeeQuery(
        String name,
        String bi,
        String nuit,
        String email,
        EnumTypes.Genre genre,
        EnumTypes.MaritalStatus maritalStatus
) {
}
