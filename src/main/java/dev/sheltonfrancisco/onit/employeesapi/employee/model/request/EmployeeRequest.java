package dev.sheltonfrancisco.onit.employeesapi.employee.model.request;

import dev.sheltonfrancisco.onit.employeesapi.utils.EnumTypes;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

import java.time.LocalDate;

@Builder
public record EmployeeRequest(
        @NotNull(message = "name must not be null")
        @NotBlank(message = "name must be filled")
        String name,
        @NotNull(message = "surname must not be null")
        @NotBlank(message = "surname must be filled")
        String surname,
        @NotNull(message = "birth date must not be null")
        LocalDate birthDate,
        @NotNull(message = "bi must not be null")
        @NotBlank(message = "bi must be filled")
        String bi,
        @NotNull(message = "nuit must not be null")
        @NotBlank(message = "nuit must be filled")
        String nuit,
        @NotNull(message = "phoneNumber must not be null")
        @NotBlank(message = "phoneNumber must be filled")
        @Pattern(regexp = "^(\\+258)8[1-7][0-9]{7}$", message = "Phone number does not match a valid pattern")
        String phoneNumber,
        @Email(message = "Email is invalid")
        @NotNull(message = "email must not be null")
        @NotBlank(message = "Email must be filled")
        String email,
        @NotNull(message = "maritalStatus must be filled")
        EnumTypes.MaritalStatus maritalStatus,
        @NotNull(message = "genre must be filled")
        EnumTypes.Genre genre
) {
}
