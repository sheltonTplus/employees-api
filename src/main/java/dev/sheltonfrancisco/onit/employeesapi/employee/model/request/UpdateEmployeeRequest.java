package dev.sheltonfrancisco.onit.employeesapi.employee.model.request;

import dev.sheltonfrancisco.onit.employeesapi.utils.EnumTypes;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

import java.time.LocalDate;

@Builder
public record UpdateEmployeeRequest(
        String name,
        String surname,
        String bi,
        String nuit,
        LocalDate birthDate,
        @Nullable
        @Email
        String email,
        @Nullable
        @Pattern(regexp = "^(\\+258)8[1-7][0-9]{7}$", message = "Phone number does not match a valid pattern")
        String phoneNumber,
        EnumTypes.Genre genre,

        EnumTypes.MaritalStatus maritalStatus
) {
}
