package dev.sheltonfrancisco.onit.employeesapi.employee.presentation;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.EmployeeMapper;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeQuery;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.UpdateEmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.service.EmployeeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService service;
    @PostMapping
    public ResponseEntity<EmployeeJson> create(@RequestBody @Valid EmployeeRequest request) {
        return new ResponseEntity<>(EmployeeMapper.INSTANCE.toJson(service.create(request)), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<EmployeeJson>> findAll(@PageableDefault(size = 15) Pageable pageable, EmployeeQuery query) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.toJson(service.findAll(pageable, query)));
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeJson> findById(@PathVariable Long id) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.toJson(service.findById(id)));
    }

    @PutMapping("{id}")
    public ResponseEntity<EmployeeJson> updateById(@PathVariable Long id, @RequestBody @Valid UpdateEmployeeRequest request) {
        return ResponseEntity.ok(EmployeeMapper.INSTANCE.toJson(service.update(request, id)));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
