package dev.sheltonfrancisco.onit.employeesapi.employee.presentation;

import lombok.Builder;

import java.time.LocalDate;

@Builder
public record EmployeeJson(
    Long id,
    String name,
    String surname,
    String bi,
    String nuit,
    String phoneNumber,
    String email,
    String genre,
    String maritalStatus,
    LocalDate birthDate) {
}
