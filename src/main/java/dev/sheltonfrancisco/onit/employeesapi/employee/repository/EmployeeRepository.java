package dev.sheltonfrancisco.onit.employeesapi.employee.repository;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EmployeeRepository extends JpaRepository<Employee, Long>,
        JpaSpecificationExecutor<Employee> {
}
