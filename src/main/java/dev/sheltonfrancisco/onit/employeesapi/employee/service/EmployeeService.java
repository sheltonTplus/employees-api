package dev.sheltonfrancisco.onit.employeesapi.employee.service;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.Employee;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeQuery;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.UpdateEmployeeRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface EmployeeService {
    Employee create(EmployeeRequest request);
    Employee findById(Long id);
    Page<Employee> findAll(Pageable pageable, EmployeeQuery query);
    Employee update(UpdateEmployeeRequest request, Long id);
    void deleteById(Long id);
}
