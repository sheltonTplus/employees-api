package dev.sheltonfrancisco.onit.employeesapi.employee.service;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.Employee;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.EmployeeMapper;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeQuery;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.UpdateEmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.repository.EmployeeRepository;
import dev.sheltonfrancisco.onit.employeesapi.exceptions.EmployeeException;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository repository;
    private final EmployeeSpecification specification;

    @Override
    public Employee create(EmployeeRequest request) {
        Employee employeeModel = EmployeeMapper.INSTANCE.toModel(request);
        try {
            return repository.save(employeeModel);
        } catch (DataIntegrityViolationException exception) {
            if (exception.getCause() instanceof ConstraintViolationException)
                throw new EmployeeException("This employee is already registered", HttpStatus.CONFLICT);

            throw new EmployeeException("An error occurred while creating employee", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public Employee findById(Long id) {
        return repository.findById(id).orElseThrow(() -> new EmployeeException("Employee not found: " + id, HttpStatus.NOT_FOUND));
    }

    @Override
    public Page<Employee> findAll(Pageable pageable, EmployeeQuery query) {
        return repository.findAll(specification.executeQuery(query), pageable);
    }

    @Override
    public Employee update(UpdateEmployeeRequest request, Long id) {
        var employee = findById(id);
        EmployeeMapper.INSTANCE.updateEmployeeFromRequest(request, employee);
        return repository.save(employee);
    }

    @Override
    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
        else throw new EmployeeException("Employee not found: " + id, HttpStatus.NOT_FOUND);
    }
}
