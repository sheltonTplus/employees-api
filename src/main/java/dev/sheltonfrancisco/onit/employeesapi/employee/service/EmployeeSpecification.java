package dev.sheltonfrancisco.onit.employeesapi.employee.service;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.Employee;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class EmployeeSpecification {

    public Specification<Employee> executeQuery(EmployeeQuery employeeQuery) {
        return all()
                .and(findByName(employeeQuery.name()))
                .and(findBy(employeeQuery.email(), "email"))
                .and(findBy(employeeQuery.bi(), "bi"))
                .and(findBy(employeeQuery.nuit(), "nuit"))
                .and(findBy(employeeQuery.maritalStatus(), "maritalStatus"))
                .and(findBy(employeeQuery.genre(), "genre"));
    }

    public Specification<Employee> findByName(String name) {
        return (root, query, cb) ->
                name == null || name.isBlank() ? null : cb.like(cb.lower(cb.concat(root.get("name"), root.get("surname"))), "%" + name.toLowerCase() + "%");
    }

    /**
     * @param value field value
     * @param field field name
     * */
    public Specification<Employee> findBy(String value, String field) {
        return (root, query, cb) -> value == null || value.isBlank() ? null : cb.equal(root.get(field), value);
    }

    /**
     * @param value field value
     * @param field field name
     * */
    public Specification<Employee> findBy(Enum value, String field) {
        return (root, query, cb) -> value == null ? null : cb.equal(root.get(field), value);
    }

    public Specification<Employee> all() {
        return ((root, query, criteriaBuilder) -> criteriaBuilder.and());
    }
}
