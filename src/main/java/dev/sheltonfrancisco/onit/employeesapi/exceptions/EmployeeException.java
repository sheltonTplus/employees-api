package dev.sheltonfrancisco.onit.employeesapi.exceptions;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public class EmployeeException extends RuntimeException {

    private String message;
    private HttpStatus httpStatus;

    public EmployeeException(String message) {
        super(message);
        this.httpStatus = HttpStatus.BAD_REQUEST;
        this.message = message;
    }
}
