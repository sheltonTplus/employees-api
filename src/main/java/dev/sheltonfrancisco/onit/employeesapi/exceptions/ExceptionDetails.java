package dev.sheltonfrancisco.onit.employeesapi.exceptions;

import lombok.Builder;
import lombok.Getter;
import java.time.LocalDateTime;

@Builder
@Getter
public class ExceptionDetails {
    private Integer statusCode;
    private LocalDateTime timestamp;
    private String message;
}
