package dev.sheltonfrancisco.onit.employeesapi.exceptions;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {EmployeeException.class})
    public ResponseEntity<ExceptionDetails> handleEmployeeException(EmployeeException exception) {
        ExceptionDetails exceptionDetails = ExceptionDetails.builder()
                .message(exception.getMessage())
                .statusCode(exception.getHttpStatus().value())
                .timestamp(LocalDateTime.now())
                .build();
        return new ResponseEntity<>(exceptionDetails, exception.getHttpStatus());
    }

    @ExceptionHandler(value = {BindException.class})
    public ResponseEntity<ExceptionDetails> handleBindException(BindException exception) {
        ExceptionDetails exceptionDetails = ExceptionDetails.builder()
                .message(exception.getAllErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .findFirst().get())
                .statusCode(HttpStatus.BAD_REQUEST.value())
                .timestamp(LocalDateTime.now())
                .build();
        return new ResponseEntity<>(exceptionDetails, HttpStatus.BAD_REQUEST);
    }
}