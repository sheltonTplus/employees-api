package dev.sheltonfrancisco.onit.employeesapi.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class EnumTypes {

    @Getter
    @AllArgsConstructor
    public enum Genre {
        MALE("Masculino"), FEMALE("Feminino");
        private final String description;
    }

    @Getter
    @AllArgsConstructor
    public enum MaritalStatus {

        MARRIED("Casado"), DIVORCED("Divorciado"), SINGLE("Solteiro");
        private final String description;
    }

    @Getter
    @AllArgsConstructor
    public enum Bank {
        BCI("BCI"),
        MILLENNIUM_BIM("Millennium BIM"),
        NED_BANK("Ned Bank"),
        STANDARD_BANK("Standard Bank"),
        MOZA_BANK("Moza Bank");

        private final String description;
    }
}
