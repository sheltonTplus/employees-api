ALTER TABLE employees
    ADD COLUMN payment_info BIGINT;

CREATE TABLE payment_info
(
    id             BIGINT PRIMARY KEY,
    account_number VARCHAR(14) NOT NULL,
    nib            VARCHAR(24) NOT NULL
);

CREATE SEQUENCE IF NOT EXISTS paymeny_info_seq INCREMENT BY 50;

ALTER TABLE employees
    ADD FOREIGN KEY (payment_info) REFERENCES payment_info (id);