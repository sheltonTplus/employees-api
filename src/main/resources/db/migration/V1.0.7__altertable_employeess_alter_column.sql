ALTER TABLE employees
    RENAME COLUMN payment_info TO payment_info_id;

ALTER TABLE payment_info
    ADD COLUMN salary MONEY