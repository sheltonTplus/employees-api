package dev.sheltonfrancisco.onit.employeesapi.employee;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.UpdateEmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.repository.EmployeeRepository;
import org.instancio.Instancio;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.instancio.Select.field;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeIntegrationTest {

    @Autowired
    private EmployeeRepository repository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    Faker faker = new Faker();

    private final String ROUTE = "/api/v1/employees";

    @Nested
    class createEmployee {
        @Test
        @DisplayName("Should Create employee with success")
        void shouldCreateEmployee() throws Exception {
            // Arrange
            var employeeRequest = Instancio.of(EmployeeRequest.class)
                    .set(field(EmployeeRequest::email), faker.internet().emailAddress())
                    .set(field(EmployeeRequest::phoneNumber), faker.regexify("(\\+258)8[1-7][0-9]{7}")).create();
            // Act and Assert
            mockMvc.perform(post(ROUTE).
                            contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(employeeRequest)))
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("$.id").exists());
        }

        @Test
        @DisplayName("Should return a bad request error")
        void shouldNotCreateEmployee() throws Exception {
            var employeeRequest = Instancio.of(EmployeeRequest.class)
                    .ignore(field(EmployeeRequest::name))
                    .ignore(field(EmployeeRequest::surname))
                    .create();

            mockMvc.perform(post(ROUTE)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(employeeRequest)))
                    .andExpect(status().isBadRequest());

        }
    }

    @Nested
    class findById {
        @Test
        @DisplayName("Should return employee by id")
        void shouldFindEmployeeById() throws Exception {
            var employeeId = repository.findAll().stream().findFirst().get().getId();

            mockMvc.perform(get(ROUTE.concat("/" + employeeId)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(employeeId));
        }

        @Test
        @DisplayName("Should return employee not found")
        void shouldNotFindEmployeeById() throws Exception {
            var employeeId = repository.findAll().size() + faker.number().randomNumber();

            mockMvc.perform(get(ROUTE.concat("/" + employeeId)))
                    .andExpect(status().isNotFound());
        }
    }

    @Nested
    class updateEmployee {

        @Test
        @DisplayName("Should update employee")
        void shouldUpdateEmployee() throws Exception {
            var fetchedEmployee = repository.findAll().stream().findAny().get();
            var updateRequest = Instancio.of(UpdateEmployeeRequest.class)
                    .ignore(field(UpdateEmployeeRequest::email))
                    .set(field(UpdateEmployeeRequest::name), faker.name().firstName())
                    .set(field(UpdateEmployeeRequest::phoneNumber), faker.regexify("(\\+258)8[1-7][0-9]{7}"))
                    .create();
            mockMvc.perform(put(ROUTE.concat("/" + fetchedEmployee.getId()))
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(updateRequest)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(fetchedEmployee.getId()));
        }
    }
}
