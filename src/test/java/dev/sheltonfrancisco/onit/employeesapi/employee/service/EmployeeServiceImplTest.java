package dev.sheltonfrancisco.onit.employeesapi.employee.service;

import dev.sheltonfrancisco.onit.employeesapi.employee.model.Employee;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.EmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.model.request.UpdateEmployeeRequest;
import dev.sheltonfrancisco.onit.employeesapi.employee.repository.EmployeeRepository;
import dev.sheltonfrancisco.onit.employeesapi.exceptions.EmployeeException;
import org.instancio.Instancio;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.Optional;

import static org.instancio.Select.field;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    private EmployeeRepository repository;
    @Mock
    private EmployeeSpecification specification;
    @InjectMocks
    private EmployeeServiceImpl underTest;
    @Captor
    private ArgumentCaptor<Employee> employeeArgumentCaptor;
    @Captor
    private ArgumentCaptor<Long> idArgumentCaptor;
    @Captor
    private ArgumentCaptor<PageRequest> pageRequestArgumentCaptor;
    private ArgumentCaptor<Specification<Employee>> specArgumentCaptor;

    @Nested
    class createEmployee {
        @Test
        @DisplayName("Should Create An Employee with Success")
        void shouldCreateEmployee() {
            // Arrange
            var employee = Instancio.of(Employee.class)
                    .ignore(field(Employee::getDeletedAt))
                    .create();
            doReturn(employee).when(repository).save(employeeArgumentCaptor.capture());
            var input = Instancio.create(EmployeeRequest.class);

            // Act
            Employee output = underTest.create(input);

            // Assert
            assertNotNull(output);
            var employeeCaptured = employeeArgumentCaptor.getValue();

            assertEquals(input.bi(), employeeCaptured.getBi());
            assertEquals(input.email(), employeeCaptured.getEmail());
            assertEquals(input.name(), employeeCaptured.getName());
            assertEquals(input.surname(), employeeCaptured.getSurname());
            assertEquals(input.nuit(), employeeCaptured.getNuit());
            assertEquals(input.phoneNumber(), employeeCaptured.getPhoneNumber());
            assertNotNull(output.getCreatedAt());
            assertNotNull(output.getUpdatedAt());
            assertNotNull(output.getId());
            assertNull(output.getDeletedAt());
        }

        @Test
        @DisplayName("Should Return a custom exception")
        void shouldReturnExceptionWhenErrorOccurs() {
            // Arrange
            doThrow(new DataIntegrityViolationException("")).when(repository).save(any());
            var input = Instancio.create(EmployeeRequest.class);

            // Act and Assert
            assertThrows(EmployeeException.class, () -> underTest.create(input));
        }
    }

    @Nested
    class findEmployeeById {

        @Test
        @DisplayName("Should find employee by id with success")
        void shouldFindEmployeeByIdWithSuccess() {
            // Arrange
            var employee = Instancio.of(Employee.class)
                    .ignore(field(Employee::getDeletedAt))
                    .create();
            doReturn(Optional.of(employee)).when(repository).findById(idArgumentCaptor.capture());

            // Act
            Employee output = underTest.findById(employee.getId());

            // Assert
            assertNotNull(output);
            assertEquals(idArgumentCaptor.getValue(), output.getId());
        }

        @Test
        @DisplayName("Should return EmployeeException when employee does not exists")
        void shouldThrowExceptionWhenEmployeeIsNotFound() {
            // Arrange
            var employeeId = Instancio.create(Long.class);
            doReturn(Optional.empty()).when(repository).findById(idArgumentCaptor.capture());

            // Act and Assert
            assertThrows(EmployeeException.class, () -> underTest.findById(employeeId));

        }
    }

//    @Nested
//    class findAllEmployees {
//
//        @Test
//        @DisplayName("Should return a")
//        void shouldReturnAPageOfEmployees() {
//            // Arrange
//            var page = mock(Page.class);
//            doReturn(page)
//                    .when(repository).findAll(specArgumentCaptor.capture(),pageRequestArgumentCaptor.capture());
//            EmployeeQuery query = Instancio.of(EmployeeQuery.class).create();
//
//            // Act
//            underTest.findAll(PageRequest.of(1, 10), query);
//
//            // Assert
//            verify(repository, times(1))
//                    .findAll(pageRequestArgumentCaptor.getValue());
//            verifyNoMoreInteractions(repository);
//        }
//    }

    @Nested
    class update {
        @Test
        @DisplayName("Should update employee by id with success")
        void shouldUpdateEmployeeById() {
            // Arrange
            var employee = Instancio.of(Employee.class)
                    .ignore(field(Employee::getDeletedAt))
                    .create();
            Long employeeId = employee.getId();
            var request = Instancio.create(UpdateEmployeeRequest.class);

            doReturn(Optional.of(employee))
                    .when(repository).findById(idArgumentCaptor.capture());
            doReturn(employee)
                    .when(repository).save(employeeArgumentCaptor.capture());

            // Act
            var output = underTest.update(request, employeeId);

            // Assert
            assertEquals(output, employeeArgumentCaptor.getValue());
            assertEquals(employeeId, idArgumentCaptor.getValue());

            verify(repository, times(1)).findById(employeeId);
            verify(repository, times(1)).save(employee);
        }

        @Test
        @DisplayName("Should throw employee not found exception")
        void shouldNotUpdate_AndShouldThrowException() {
            // Arrange
            Long employeeId = Instancio.create(Long.class);
            var request = Instancio.create(UpdateEmployeeRequest.class);

            doReturn(Optional.empty())
                    .when(repository).findById(idArgumentCaptor.capture());

            // Act and Assert
            assertThrows(EmployeeException.class, () -> underTest.update(request, employeeId));
            assertEquals(employeeId, idArgumentCaptor.getValue());

            verify(repository, times(1)).findById(employeeId);
        }
    }

    @Nested
    class deleteById {
        @Test
        @DisplayName("Should delete employee with success if exists")
        void deleteEmployeeByIdIfExists() {
            // Arrange
            doReturn(true)
                    .when(repository).existsById(idArgumentCaptor.capture());
            doNothing().when(repository).deleteById(idArgumentCaptor.capture());

            var id = Instancio.create(Long.class);

            // Act
            underTest.deleteById(id);

            // Assert
            var idList = idArgumentCaptor.getAllValues();
            assertEquals(id, idList.get(0));
            assertEquals(id, idList.get(1));

            verify(repository, times(1)).existsById(idList.get(1));
            verify(repository, times(1)).deleteById(idList.get(1));
        }

        @Test
        @DisplayName("Should thrown en error when employee does not exists")
        void shouldNotDeleteWhenEmployeeIsNotFound() {
            // Arrange
            doReturn(false)
                    .when(repository).existsById(idArgumentCaptor.capture());
            var id = Instancio.create(Long.class);

            // Act & Assert
            assertThrows(EmployeeException.class, () -> underTest.deleteById(id));
        }
    }
}